import * as React from "react";
import {View, Text, Button, FlatList, StyleSheet, ScrollView} from "react-native";

let globalStyles = require('../../assets/style/global');

export default function DetailsScreen({navigation}) {

    const userNames = [
        {key: 'Allan'},
        {key: 'Alexandre'},
        {key: 'Alexia'},
        {key: 'Benjamin'},
        {key: 'Camille'},
        {key: 'Guillaume'},
        {key: 'Jules'},
        {key: 'Kevin'},
        {key: 'Marcel'},
        {key: 'Marine'},
        {key: 'Maxime'},
        {key: 'Nicolas'},
        {key: 'Sylvain'},
        {key: 'Theau'},
        {key: 'Wendy'},

    ];

    return (
        <ScrollView style={styles.listContainer}>
            <Text style={styles.title}>La team drosalys : </Text>
            <Text style={[globalStyle.textCenter]}>Quick example of a list loop, with navigation to another page with
                parameter</Text>
            {userNames.map(
                userName =>
                    <View style={styles.button} key={userName.key}>
                        <Button title={userName.key}
                                style={styles.button}
                                key={userName.key}
                                onPress={() => {
                                    navigation.navigate('DetailsChildScreen', {name: userName.key})
                                }}
                        />
                    </View>
            )}
        </ScrollView>
    )


}

const globalStyle = StyleSheet.create(globalStyles);

const styles = StyleSheet.create({
    listContainer: {
        padding: 5,
    },

    title: {
        textAlign: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 25,
        fontWeight: 'bold',
    },

    button: {
        marginTop: 5,
        marginBottom: 5
    }
})