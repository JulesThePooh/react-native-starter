import * as React from "react";
import {View, Text} from "react-native";

export default function DetailsScreen({navigation, route}) {
    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: route.params.name === '' ? 'No title' : route.params.name,
        });
    }, [navigation, route.params.name]);

    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text
                style={{fontSize: 26, fontWeight: 'bold'}}
            >
                Page de {route.params.name}
            </Text>
        </View>
    )
}