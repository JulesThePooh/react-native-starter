import * as React from "react";
import {View, Text, Switch, StyleSheet, Button} from "react-native";
import {useState} from "react";

export default function DemoScreen({navigation}) {

    const [switchNotification, setSwitchNotification] = useState(false);

    return (

        <View style={[styles.container]}>

            <View style={{marginTop: 5, marginBottom: 5}}>
                <Button
                    title={"Demo Notifications"}
                    color="#f39c12"
                />
            </View>
            <View style={{marginTop: 5, marginBottom: 5}}>
                <Button
                    title={"Demo camera simple"}
                    color="#f39c12"
                    onPress={() => {
                        navigation.navigate('DemoCamera')
                    }}
                />
            </View>
            <View style={{marginTop: 5, marginBottom: 5}}>
                <Button
                    title={"Demo scan QR code"}
                    color="#f39c12"
                    onPress={() => {
                        navigation.navigate('DemoScanQrCode')
                    }}
                />
            </View>
            <View style={{marginTop: 5, marginBottom: 5}}>
                <Button
                    title={"Demo micro"}
                    color="#f39c12"
                    onPress={() => {
                        navigation.navigate('DemoMicro')
                    }}
                />
            </View>
            <View style={{marginTop: 5, marginBottom: 5}}>
                <Button title={"Demo map"} color="#f39c12"/>
            </View>
            <View style={{marginTop: 5, marginBottom: 5}}>
                <Button title={"Demo form"} color="#f39c12"/>
            </View>


            {/*<Text style={styles.textStyle}>Notifcations</Text>*/}
            {/*<Switch*/}
            {/*    value={switchNotification}*/}
            {/*    onValueChange={() => {*/}
            {/*        if (!switchNotification) {*/}
            {/*            setSwitchNotification(true);*/}

            {/*            //todo trigger notification with expo*/}
            {/*        } else {*/}
            {/*            setSwitchNotification(false);*/}
            {/*        }*/}
            {/*    }}*/}
            {/*/>*/}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        margin: 5,
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#344953'
    },
})