import * as React from "react";
import {Camera, CameraType} from 'expo-camera';
import {View, Text, Switch, StyleSheet, Button, TouchableOpacity, absoluteFill} from "react-native";
import {useEffect, useState} from "react";
import { BarCodeScanner } from 'expo-barcode-scanner';

export default function DemoScanQrCode({navigation}) {

    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "Scan Qr Code",
        });
    }, [navigation, "Scan Qr Code"]);

    useEffect(() => {
        const getBarCodeScannerPermissions = async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        };

        getBarCodeScannerPermissions();
    }, []);

    const handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);
        alert("Text of the QR code is : " + data);
    };

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }




    return (
        <View style={styles.container}>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFill}
            />

            {scanned && <View style={{marginBottom: 10}}><Button title={'Click to scan again'} onPress={() => setScanned(false)} /></View>}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
});