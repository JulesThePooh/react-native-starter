import * as React from "react";
import {Camera, CameraType} from 'expo-camera';
import {View, Text, Switch, StyleSheet, Button, TouchableOpacity} from "react-native";
import {useState} from "react";

export default function DemoCameraScreen({navigation}) {

    //type of the camera back or front
    const [type, setType] = useState(CameraType.back);

    //used to check the permission of the user camera
    const [permission, requestPermission] = Camera.useCameraPermissions();

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "Camera",
        });
    }, [navigation, "Camera"]);

    //si pas de permission en rend qqch
    if (!permission) {
        // Camera permissions are still loading
        return <View />;
    }

    //si permission n'est pas permise, button pour l'accepter
    if (!permission.granted) {
        // Camera permissions are not granted yet
        return (
            <View>
                <Text style={{ textAlign: 'center' }}>
                    We need your permission to show the camera
                </Text>
                <Button onPress={requestPermission} title="grant permission" />
            </View>
        );
    }


    //function to change the type of the camera
    function toggleCameraType() {
        setType((current) => (
            current === CameraType.back ? CameraType.front : CameraType.back
        ));
    }

    return (
        <View style={styles.container}>

            {/*actual camera item*/}
            <Camera style={styles.camera} type={type}>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={toggleCameraType}>
                        <Text style={styles.text}>Flip Camera</Text>
                    </TouchableOpacity>
                </View>
            </Camera>



        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    camera: {
        flex: 1,
    },
    buttonContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        margin: 64,
    },
    button: {
        flex: 1,
        alignSelf: 'flex-end',
        alignItems: 'center',
    },
    text: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'white',
    },
});