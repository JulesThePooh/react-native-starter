import * as React from "react";
import {View, Text, Switch, StyleSheet} from "react-native";


export default function SettingsScreen({navigation}) {


    return (
        <View style={styles.container}>
            <Text>Settings</Text>
            <Text onPress={() => {
                navigation.navigate('Home')
            }}>Click on me tgo back home</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        margin: 5,
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#344953'
    }
})