import * as React from "react";
import {View, Text, StyleSheet} from "react-native";

//import global style
let globalStyle = require('../../assets/style/global');

export default function HomeScreen({navigation}) {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text
                onPress={() => alert('Ceci est la Home Screen')}
                style={{fontSize: 26, fontWeight: 'bold'}}
            >
                Home Screen <Text style={{fontSize: 8}}>Click on me for an alert</Text>
            </Text>
            <View style={{padding: 5}}>
                <Text style={[styles.textRed, styles.textCenter]}>
                    I am using global style !
                </Text>
                <Text style={[styles.textRed, styles.textCenter]}>
                    see ./style/base/text.js
                </Text>
                <Text style={[styles.textRed, styles.textCenter]}>
                    imported in ./src/style/global.js
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create(globalStyle);