import * as React from "react";
import {View, Text, StyleSheet, ScrollView, Image, Button, ActivityIndicator, Modal} from "react-native";
let globalStyles = require('../../assets/style/global');

import {useEffect, useState} from "react";

//you can import .env variable like this
import {STEAMISH_URL} from "@env";


export default function DetailsScreen({navigation}) {

    //variable and their setter
    //state is used for dynamic variable in your code, like angular dynamic variable system
    const [isLoading, setIsLoading] = useState(false);
    const [items, setItems] = useState([]);
    const [page, setPage] = useState(1);
    const [hasMoreData, setHasMoreData] = useState(true);


    //function used to check if the user scrolle to the bottom
    const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    //useEffect is trigger before mount the react component
    useEffect(() => {
        fetchGame();
    }, [])


    return (
        //Scroll View is used for a scrollable screen, onScroll eventListner on each scroll
        <ScrollView style={{padding: 5}} onScroll={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
                fetchGame();
            }
        }}>
            <View>
                <Text style={[globalStyle.textSmall, globalStyle.textCenter]}>
                    On this page there is an api call example, a scroll bottom trigger event with api call and a loader example.
                </Text>
            </View>
            {/*The right way to loop other variable with react (.map)*/}
            {items.map((game, index) =>
                <View style={[styles.card, styles.shadowProp]} key={index}>
                    <Image
                        source={{uri: game.thumbnailCover}}
                        resizeMethod='resize'
                        style={{width: "100%", height: 300}}
                    />

                    <View style={{padding: 15}}>
                        <Text style={{fontWeight: 'bold', fontSize: 20, textAlign: 'center'}}>{game.name}</Text>
                        <Text style={{marginTop: 15}}>{game.description}</Text>
                        <View style={{marginTop: 30}}>
                            <Button
                                title={'Detail du jeu'}
                                color="#f39c12"
                                onPress={() => {
                                    navigation.navigate('Home')
                                }}
                            />
                        </View>
                    </View>
                </View>
            )}
            {
                //modal will be visible by a state bool (isLoading)
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={isLoading}
                >
                    <View style={styles.centeredView}>
                        <ActivityIndicator size="large"/>
                    </View>
                </Modal>
            }
        </ScrollView>
    )


    //javascript native function to ajax
    function fetchGame(search = null) {
        if (hasMoreData) {
            if (!isLoading) {
                setIsLoading(true);

                fetch(STEAMISH_URL + "game?page=" + page).then((response) => {
                    return response.json()
                }).then((result) => {

                    if (result.items.length === 0) {
                        setHasMoreData(false);
                    }
                    result.items.forEach((game) => {
                        setItems(oldItems => [...oldItems, game]);
                    })
                    setIsLoading(false);
                    setPage(oldVal => oldVal + 1);
                }).catch((error) => {
                    setIsLoading(false);
                })
            }
        }
    }


}

const globalStyle = StyleSheet.create(globalStyles);

//style used specific for this pages
//you can call them like this in any React attribute above : style={[styles.card, styles.shadowProp]}
const styles = StyleSheet.create({
    heading: {
        fontSize: 18,
        fontWeight: '600',
        marginBottom: 13,
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 8,
        paddingVertical: 35,
        paddingHorizontal: 15,
        width: '100%',
        marginVertical: 10,
    },
    shadowProp: {
        shadowColor: '#171717',
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
        paddingBottom: 80
    }
});