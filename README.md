# React Native Starter

### Quick infos :
    - This project is using expo, what is expo:
    - Expo is a bundles of tools that help you start a project fast
    - you don't need to have android studio or any stuff installed on your computer
    - this project have ionicons installed, you can find the list in : https://ionic.io/ionicons/v4

### What do I need to start the project ?
    - You need to have a smartphone with Android or IOS
    - download Expo GO from playStore or appStore
    - your computer and your phone have to be on the same connexion (same WIFI for example)
    
    - You need to have node 16.0.0 or above
    - You need to have the lastest version of NPM you can run : npm install -g npm@lates
    - That's it !

### How do i start the project 
    - Run npm install
    - Run npm start, you will have a QRCode like this :

![Screenshot](read_me_screen/qr_code.png)

    - Run the app Expo GO from your phone
    - Select Scan QR code, and scan the code
    - You are good to go !


### What comes with this starter ?
    - This starter is very useful for having a simple example of a running app
    - Routes are already configurated

### How to add a route in the tab bar ?
    - Go to route-configurator/TabRoutes, import your jsFile like this : 
![Screenshot](read_me_screen/import.png)

    - And then add the configuration in the constante ROUTES_TAB_BAR like this :
![Screenshot](read_me_screen/use.png)

    - name: will be the route name, if you wanna redirect on it on your app
    - component: the js file that will be rendered
    - iconFocused: icon that will be display if focus
    - iconNotFocused: icon that will be display if not focused
    - title: The main title (header display) of your page
    

### How to create a route that is not display in the tab Bar and navigate to it ?
    - Go to route-configurator/MainRoutes.js, import your JS file like this : 
![Screenshot](read_me_screen/import_simple.png)

    - Configure your route in the constante SIMPLE_ROUTES like this :
![Screenshot](read_me_screen/config_simple.png)

    - Now you can use this route anywhere in you app like this (full example in screens/Details/DetailsScreen.js) : 
![Screenshot](read_me_screen/export_with_nav.png)
![Screenshot](read_me_screen/navigat_simple.png)

### How to navigate to a route with parameters ?
    - use the same navigation as above, but add parameter like this : 
![Screenshot](read_me_screen/navigate_param.png)

    - you can retrieve it in your other file like this (full_example in screens/Details/DetailsChildScreen.js) : 
![Screenshot](read_me_screen/retrieve.png)