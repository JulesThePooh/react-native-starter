import * as React from "react";
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from "@react-navigation/native";


/**
 * Tab importation (they WILL BE displayed in the tab !)
 */
import TabRoutes from "./TabRoutes";


/**
 * import simple route (they WILL NOT BE displayed in the tab !)
 */
import DetailsChildScreen from "../screens/Details/DetailsChildScreen";
import DemoScanQrCodeScreen from "../screens/Demo/DemoScanQrCodeScreen";
import DemoCameraScreen from "../screens/Demo/DemoCameraScreen";
import DetailsMicroScreen from "../screens/Demo/DemoMicroScreen";

/**
 * add your simple route on the under array to be configured
 */
const SIMPLE_ROUTES = [
    {name: "DetailsChildScreen", component: DetailsChildScreen},
    {name: "DemoScanQrCode", component: DemoScanQrCodeScreen},
    {name: "DemoCamera", component: DemoCameraScreen},
    {name: "DemoMicro", component: DetailsMicroScreen},
]

const Stack = createNativeStackNavigator();

export default function MainRoutes() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="TabBar"
                    component={TabRoutes}
                    options={{headerShown: false}}
                />

                {SIMPLE_ROUTES.map(
                    (simpleRoute, index) => <Stack.Screen
                        name={simpleRoute.name}
                        component={simpleRoute.component}
                        key={index}
                    />
                )}
            </Stack.Navigator>
        </NavigationContainer>
    )
}