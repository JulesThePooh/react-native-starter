import * as React from "react";

import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import IonIcons from 'react-native-vector-icons/Ionicons';

/**
 * Import route for your tab
 */
import HomeScreen from "../screens/Home/HomeScreen";
import DetailsScreen from "../screens/Details/DetailsScreen";
import SettingsScreen from "../screens/Settings/SettingsScreen";
import GameScreen from "../screens/Game/GameScreen";
import DemoScreen from "../screens/Demo/DemoScreen";

/**
 * add your tab route on the under array
 */
const ROUTES_TAB_BAR = [
    {name: "Home", component: HomeScreen, iconFocused: "home", iconNotFocused: "home-outline", title: "Acceuil"},
    {
        name: "Details",
        component: DetailsScreen,
        iconFocused: "list",
        iconNotFocused: "list-outline",
        title: "Drosalys Team"
    },
    {
        name: "Game",
        component: GameScreen,
        iconFocused: "game-controller",
        iconNotFocused: "game-controller-outline",
        title: "Game Api"
    },
    {name: "Demo", component: DemoScreen, iconFocused: "book", iconNotFocused: "book-outline", title: "Demo"},
    {
        name: "Settings",
        component: SettingsScreen,
        iconFocused: "settings",
        iconNotFocused: "settings-outline",
        title: "Config"
    },
]

const Tab = createBottomTabNavigator();

export default function TabRoutes() {
    return (
        <Tab.Navigator

            //-----------you can define initial route here (like getInitialTab("Demo"))-------------\\
            initialRouteName={getInitialTab()}
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;
                    let rn = route.name;

                    //check current route and get the good icon
                    ROUTES_TAB_BAR.forEach((routeTabBar) => {
                        if (rn === routeTabBar.name) {
                            iconName = focused ? routeTabBar.iconFocused : routeTabBar.iconNotFocused
                        }
                    })

                    return <IonIcons name={iconName} size={size} color={color}/>
                },
                tabBarInactiveTintColor: '#bdc3c7',
                tabBarActiveTintColor: '#e74c3c',
                tabBarLabelStyle: {paddingBottom: 10, fontSize: 10},
                tabBarStyle: {padding: 10, height: 70}
            })}

        >

            {ROUTES_TAB_BAR.map(
                (routeTabBar, index) => <Tab.Screen
                    name={routeTabBar.name}
                    component={routeTabBar.component}
                    options={{title: routeTabBar.title}}
                    key={index}
                />
            )}

        </Tab.Navigator>
    )
}

function getInitialTab(initialComponent = null) {
    if (initialComponent !== null) {
        return initialComponent;
    } else {
        return ROUTES_TAB_BAR[0].name;
    }
}