import * as React from "react";
import MainRoutes from "./route-configurator/MainRoutes";

export default function App() {
    return (
        <MainRoutes/>
    )
}