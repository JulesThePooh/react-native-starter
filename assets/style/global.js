//import your style file
let buttonStyle = require('./base/button');
let textStyle = require('./base/text');

//add your variable into this array
let importedStyles = [
    buttonStyle,
    textStyle,
]

let obj = {}

importedStyles.forEach((importedStyle) => {
    for (const [key, value] of Object.entries(importedStyle)) {
        obj[key] = value;
    }
})

module.exports = obj;