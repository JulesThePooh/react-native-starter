module.exports = {
    textRed: {
        color: '#e74c3c',
    },
    textSmall: {
        fontSize: 10,
    },
    textCenter: {
        textAlign: 'center',
    }
}